
const webdriver  = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');

const co = new chrome.Options().setChromeBinaryPath('C:\\Program Files (x86)\\BraveSoftware\\Brave-Browser\\Application\\brave.exe')

openURL = async() => {

   let driver = await new webdriver.Builder()
   .forBrowser('chrome')
   .setChromeOptions(co)
   .build();

   try {
   
      await driver.get('http://www.centercell.com.br/')

      const cheese = await driver.findElement(webdriver.By.css('#span_consulteaqui'));
      
      cheese.click()
   
   } catch (e) {
      console.log('Erro ao abrir website: ' + e);
      driver.quit()
   }

}

openURL()