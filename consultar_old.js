const sql = require('mssql')
const fs = require('fs')
const conexao = require('./connDB')
const request = require('node-fetch')
const url = 'https://ing.care-br.com/ing_api.php'
const json_status1 = {
    "user":"centercell",
    "key":"$2a$98$MyY3MTk2NzIzNzU3MjIwM.vDy.uGKbTBZzauoWKFS0dvdi6YVA8AG",
    "method":"Consulta_Pedido",
    "request":{
        "Status":"1"
    }
}

const json_status3 = {
    "user":"centercell",
    "key":"$2a$98$MyY3MTk2NzIzNzU3MjIwM.vDy.uGKbTBZzauoWKFS0dvdi6YVA8AG",
    "method":"Consulta_Pedido",
    "request":{
        "Status":"3"
    }
}

const getOs = os =>
`
select top 1
xpc_cabecAtendimentoVD.osCare
from
xpc_cabecAtendimentoVD (nolock)
where
xpc_cabecAtendimentoVD.osCare = '${os}'
and
xpc_cabecAtendimentoVD.ativo = 1
order by xpc_cabecAtendimentoVD.idCadastro desc
`

const updateOS = 
`
update
xpc_cabecAtendimentoVD
set
xpc_cabecAtendimentoVD.idStatus = 2
from
xpc_cabecAtendimentoVD
where
xpc_cabecAtendimentoVD.idStatus = 1
and
xpc_cabecAtendimentoVD.ativo = 1
`

erro = false
idCabec = ''

function encerra(e){
    console.log(e);
    process.exit();
}

const executar = async() => {

    //await consultaStatus1(json_status1)

    await consultaStatus3(json_status3)

    if(erro == true){
        encerra('API OK - Verifique os erros!')
    }else{
        encerra('API OK')
    }

}

const consultaStatus1 = async(json) => {

    const data = JSON.stringify(json)

    const resposta = await request(url,{
        method:'post',
        body:data,
        headers:{'Content-Type': 'application/json'}
    })

    dados = await resposta.json()

    if(dados.retorno == 'false'){
        erro = true
        await gravaErro(data,dados.mensagem)
    }else{

        osOK = []
        var listaOS = []

        for (const x of dados.pedidos) { 
            
            listaOS.push(x['OS'])
        
        }

        console.log('Total de chamados: ' + dados.pedidos.length)

        const query = listaOS.map((i) => consulta(i))
        await Promise.all(query)

        console.log('Query OK!')

        dados.pedidos = dados.pedidos.filter(obj => !osOK.includes(obj.OS))

        console.log('Pedidos analisados!');
        
        if(dados.pedidos.length > 0){
            console.log('Realizando BULK!');
            await bulk(dados)    
            await exec(updateOS) 
        }else{
            erro = true
            await gravaErro(data,'N�o existem novos chamados para recebermos!')
        }

    }

}

const consultaStatus3 = async(json) => {

    const data = JSON.stringify(json)

    const resposta = await request(url,{
        method:'post',
        body:data,
        headers:{'Content-Type': 'application/json'}
    })

    const dados = await resposta.json()

    if(dados.retorno == 'false'){
        erro = true
        await gravaErro(data,dados)
    }else{

        var qry = []
        
       for (const j of dados.pedidos) {

            let osCare = j['OS']
            let serial = j['PRODUTO_SERIAL']
            let modelo = j['PRODUTO_CODIGO']
            let nf1 = j['NF']
            let objeto = j['ETICKET']
            let dtNf = j['DATA_NF'].substr(0, 10)
            let dtPostagem = j['DATA_POSTAGEM']
            let idOpv = ''

            if (j['clienteNome'] = 'GETNET') {
                idOpv = '574'
            }else{
                idOpv = '682'
            }

            // const getModelo = 
            // `
            //     select top 1
            //     a.idModelo
            //     from
            //     xpc_modelosMulti a
            //     where
            //     a.modelo = '${modelo}'
            //     and
            //     a.ativo = 1
            //     order by a.idModelo desc
            // `

            // let idModelo = await exec(getModelo)

            qry.push(
                `
                    update
                    xpc_cabecAtendimentoVD
                    set
                    xpc_cabecAtendimentoVD.idStatus = 3,
                    xpc_cabecAtendimentoVD.objetoCorreio = '${objeto}',
                    xpc_cabecAtendimentoVD.dtPostagem = '${dtPostagem}'
                    from
                    xpc_cabecAtendimentoVD
                    where
                    xpc_cabecAtendimentoVD.osCare = '${osCare}'
                `

            )

            // ,

            //     `
            //         INSERT INTO 
            //         dbo.xpc_PreRecebimentoCIELO
            //         (
            //         idUser,
            //         NumeroSerie,
            //         NotaFiscal,
            //         Defeitos,
            //         idTipoAtendimento,
            //         idParceiro,
            //         userHp
            //         ) 
            //         VALUES (
            //         '199',
            //         '${serial}',
            //         '${nf1}',
            //         'N/A',
            //         '10',
            //         ${idOpv},
            //         'Integra��o'
            //         )
            //     `
            // ,
            //     `
            //     set dateformat dmY

            //     INSERT INTO xpc_ControleRecebimentoIngCabec
            //     (
            //         idTipoStatus,
            //         numNF,
            //         idClienteOPV,
            //         qtdTotal,
            //         idTipoProcesso,
            //         Objeto,
            //         dtNF1
            //     ) VALUES (
            //         8,
            //         '${osCare}',
            //         ${idOpv},
            //         '1',
            //         '12',
            //         '${objeto}',
            //         '${dtNf}'
            //     )

            //     select scope_identity() as id
            // `
            // )

        }   
        
        //console.log(qry);

        const insert = qry.map((q,i) => processar(q,i))

        await Promise.all(insert,)

        console.log('QUERY OK!');
        
    }

}

const gravaErro = async(jsonErro,msgErro) => {

    const queryErro = 

    `
    
    insert into xpc_logIntegracaoCare
    (
        enviado,
        erro
    )
    values
    (
        '${JSON.stringify(jsonErro)}',
        '${JSON.stringify(msgErro)}'
    )
    `

   let dds = await exec(queryErro)

}

const bulk = async(json) => {

    const tabela = new sql.Table('xpc_cabecAtendimentoVD')
    tabela.create = false
    
    tabela.columns.add('osCare', sql.VarChar(30), {nullable:false})
    tabela.columns.add('pedidoVenda', sql.VarChar(30), {nullable:false})
    tabela.columns.add('numnf', sql.VarChar(30), {nullable:true})
    tabela.columns.add('serial', sql.VarChar(30), {nullable:false})
    tabela.columns.add('produto', sql.VarChar(50), {nullable:false})
    tabela.columns.add('descProduto', sql.VarChar(50), {nullable:false})
    tabela.columns.add('dtChamado', sql.VarChar(20), {nullable:false})
    tabela.columns.add('clienteNome', sql.VarChar(1000), {nullable:false})
    tabela.columns.add('consumidorNome', sql.VarChar(1000), {nullable:false})
    tabela.columns.add('cgc', sql.VarChar(14), {nullable:false})
    tabela.columns.add('cep', sql.VarChar(8), {nullable:false})
    tabela.columns.add('endereco', sql.VarChar(500), {nullable:false})
    tabela.columns.add('numero', sql.VarChar(30), {nullable:false})
    tabela.columns.add('bairro', sql.VarChar(200), {nullable:false})
    tabela.columns.add('complemento', sql.VarChar(300), {nullable:true})
    tabela.columns.add('cidade', sql.VarChar(200), {nullable:false})
    tabela.columns.add('estado', sql.Char(2), {nullable:false})
    tabela.columns.add('clienteEmail', sql.VarChar(200), {nullable:true})
    tabela.columns.add('autoPostagem', sql.VarChar(30), {nullable:true})
    tabela.columns.add('dtPostagem', sql.VarChar(20), {nullable:true})
    tabela.columns.add('eticket', sql.VarChar(30), {nullable:true})

    for (const dadosBulk of json.pedidos) {

        tabela.rows.add(
            
            dadosBulk['OS'],
            dadosBulk['PEDIDO'],
            dadosBulk['NF'],
            dadosBulk['PRODUTO_SERIAL'],
            dadosBulk['PRODUTO_CODIGO'],
            dadosBulk['PRODUTO_DESC'],
            dadosBulk['DATA_OS'],
            dadosBulk['CLIENTE'],
            dadosBulk['CONSUMIDOR_NOME'],
            dadosBulk['CONSUMIDOR_CPF'],
            dadosBulk['CONSUMIDOR_CEP'],
            dadosBulk['CONSUMIDOR_ENDERECO'],
            dadosBulk['CONSUMIDOR_NUMERO'],
            dadosBulk['CONSUMIDOR_BAIRRO'],
            dadosBulk['CONSUMIDOR_COMPLEMENTO'],
            dadosBulk['CONSUMIDOR_CIDADE'],
            dadosBulk['CONSUMIDOR_UF'],
            dadosBulk['CONSUMIDOR_EMAIL'],
            dadosBulk['AUT_POSTAGEM'],
            dadosBulk['DATA_POSTAGEM'],
            dadosBulk['ETICKET']

        )
        
    }   

    await sql.connect(conexao.IngenicoTeste)

    .then(async() => {
        const req = new sql.Request()
        await req.bulk(tabela)
        .then(() => console.log('Pedidos recebidos!'))
        .catch((err) => {
            console.log('Erro ao processar BULK: ' +  err);
        })
    })
    .catch(err => {
        console.log('Erro ao conectar no BD: ' + err)
    })
}

const exec = (query) => {
    
    return new Promise((res,rej)=>{

        new sql.ConnectionPool(conexao.IngenicoTeste).connect()

        .then(pool => {
            res(pool.query(query))
        })
        .catch(er => {
            rej(er)
        })

    })

}

const processar = (qry,idx) => {

    return new Promise((res,rej) =>{

        const pool = new sql.ConnectionPool(conexao.IngenicoTeste)

        pool.connect()

        .then(a => {

            const transacao = new sql.Transaction(pool)

            transacao.begin(async(e) => {

                if(e){
                    rej(e)
                }

                const request = new sql.Request(transacao)
                
                request.query(qry,(erroQry,ok)=>{

                    if(erroQry){
                        transacao.rollback(errolback => {
                            rej(errolback)
                        })
                    } 

                    // request.input('idCabec',ok.recordset[0].id)
                    
                    // request.query(...qryLogs[idx],errLogs =>{
                    //     if(errLogs){
                    //         transacao.rollback((errRollSub)=>{
                    //             rej(errRollSub)
                    //         })
                    //     }else{
                    //         transacao.commit(erroCmt => {
                    //             res('OK')
                    //         })
                    //     }  
                    // })   

                }) 

                transacao.commit(erroCmt => {
                    res('OK')
                })
                
            })
        })

        .catch(erro => {
            rej(erro)
        })
        

    })

}

const consulta = (os) =>
	new Promise((resolve, reject) => {
        const pool = new sql.ConnectionPool(conexao.IngenicoTeste);
        pool.connect()
        .then(ok => {
            pool.query(getOs(os), (errQue, res) => {
                if(errQue){
                    reject(errQue)
                }else{
                    
                    if(res.rowsAffected > 0){
                        osOK.push(res.recordset[0]['osCare'])
                    }
                    resolve('OK')
                }
                
            })
        })
        .catch(fail => {
            console.log(fail);
        });
    });

executar()