// index.js
const receber = require('./consultar.js');
const atualizar = require('./atualizar.js');

async function executar() {
    console.log('* INICIANDO EXECU��O *');
    console.log('* RECEBENDO PEDIDOS *');
    await receber();
    console.log('* ATUALIZAR PEDIDOS *');
    await atualizar();
    console.log('* FIM *');
}


executar()