const express = require("express");
const {rastro:correios} = require("rastrojs");
//const obj = "OI443279699BR";
const app = express();
const port = 3000;

const start = (objeto) =>

  new Promise((resolve, reject) => {
    correios
      .track(objeto)

      .then(res =>
        resolve(res)
      )

      .catch(rej =>
        reject(rej)
      );

  });

app.get("/", (req, res) => {
  res.send("Servidor OK!");
});

app.get("/consultar/:objeto", async (req, res) => {

  var obj = req.params['objeto']

  let resultado = await start(obj);

  let json = resultado[0].tracks.slice(-1).pop() 

  json.Local = json.locale
  json.Status = json.status
  json.Obs = json.observation
  json.Data = json.trackedAt
  delete json.locale
  delete json.status
  delete json.observation
  delete json.trackedAt

  let table = '<table>'

  for(let el in json){
    table += `<tr>
                <td>
                  ${el}: ${json[el]} 
                </td>
              </tr>`
  }

  table += `</table>`

  res.send(table);

});

app.listen(port, err => {

  if (err) {
    return console.log("something bad happened", err);
  }

  console.log(`server is listening on ${port}`);

});
