const sql = require('mssql')
const conexao = require('./connDB')
const request = require('node-fetch')
const url = 'https://ing.care-br.com/ing_api.php'

const json = {
    "user":"centercell",
    "key":"$2a$98$MyY3MTk2NzIzNzU3MjIwM.vDy.uGKbTBZzauoWKFS0dvdi6YVA8AG",
    "method":"Consulta_Pedido",
    "request":{
        "Status":"5",
        "filtrar_nf2_vazia":"sim"
    }
}

const iniciar = async() => {

    console.log('* Consultando pedidos com NF2 *');
    
    await consultarNF(json)

}

const consultarNF = async(json) => {

    const data = JSON.stringify(json)

    const resposta = await request(url,{
        method:'post',
        body:data,
        headers:{'Content-Type': 'application/json'}
    })

    const dados = await resposta.json()

    for (const nf2 of dados.pedidos) {

        console.log('OSCARE: ' + nf2['OS'] + ' - ' + ' NF2: ' + nf2['NF2'] + ' - ' + ' Objeto: ' + nf2['ETICKET']);

    }

}

const processar = async(osCare,dtPostagem,objeto,serial,idOpv,dtNf,idModelo,idProcesso) => {

    return new Promise((res,rej) =>{
        
        const pool = new sql.ConnectionPool(conexao.IngenicoProd)

        pool.connect()

        .then(a => {

            const transacao = new sql.Transaction(pool)

            let idCabec = ''

            transacao.begin(async(e) => {

                if(e){
                    rej(e)
                }

                try {

                    const request = new sql.Request(transacao)
                    
                    await request.query(updateStatus3(osCare,objeto,dtPostagem))
                    await request.query(insertSinalizacao(serial,osCare,idOpv))
                    await request.query(insertCabec(osCare,idOpv,objeto,dtNf,idProcesso))
                    .then((e)=>{
                        if(e.recordset != undefined){
                            idCabec = e.recordset[0]['idCabec']
                        }
                    })
    
                    await request.query(insertItens(idCabec,idModelo))
                    await request.query(insertLog(idCabec,'8','NF Recebida por integração'))
                    //await request.query(insertLog(idCabec,'2','Integração OK - Aguardando Conferência'))
    
                    await request.query(updateStatus4(osCare))
    
                    await transacao.commit(() => {
                        res('Pedido Recebido com sucesso!')
                    })
                    
                } catch (er) {
                    transacao.rollback(erro2 => {
                        console.log(erro2 + er);
                    })
                }
                
            })
        })

        .catch(erro => {
            rej(erro)
        })
        

    })

}


iniciar()