const sql = require('mssql');

const cabec = [77, 44, 33, 22, 44, 33, 11, 12];

const queryString = value => `
    insert into tbl_cabec (cabec) values (${value})
    select scope_identity() as id
    `;

const subQueryString = value => `
    insert into tbl_item (idcabec, item) values (${value}, 876)
    `;

const insert = value =>
	new Promise((resolve, reject) => {
		const pool = new sql.ConnectionPool(config);
		pool.connect()
			.then(ok => {
				const transaction = new sql.Transaction(pool);
				transaction.begin(errBeg => {
					const req = new sql.Request(transaction);
					req.query(queryString(value), (errQue, res) => {
						if (errQue) {
							transaction.rollback(errRoll => {
								rej('rolledback');
							});
						}
						console.log(res);
						req.query(subQueryString(res.recordset[0].id), errSubQue => {
							if (errSubQue) {
								transaction.rollback(errRollSub => {
									rej('rolledback');
								});
							}

							transaction.commit(errComm => {
								resolve('job is done');
							});
						});
					});
				});
			})
			.catch(wrong => {
				console.log(wrong);
			});
	});

(() => {
	const allInserts = cabec.map(i => insert(i));
	Promise.all(allInserts);
})();
